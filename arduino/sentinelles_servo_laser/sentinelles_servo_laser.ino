/*
 * SERIAL CMD > Servo + Laser
 * Servo Shield Adafruit 16channel
 *
 * Wiring
 * D9~ : mosfets 5V - Laser 
 *
 * Servo :
 * 0 : Rotation principale
 * 1 : Rotation symétrique des deux lasers
 * 2 : Rotation un seul laser
*/

#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>

Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();

#define LASER_PIN 9
#define SERVO_FREQ 50  // Analog servos run at ~50 Hz updates,

// Serial init
int cmdId = 0;
int cmdVal = 0;

void setup() {
  Serial.begin(38400);
  pinMode(LASER_PIN, OUTPUT);

  // Servo Shield Setup
  pwm.begin();
  pwm.setOscillatorFrequency(27000000); // 23-27MHz?
  pwm.setPWMFreq(SERVO_FREQ);
  delay(100);
}

void loop() {}

void serialEvent() {
  while (Serial.available() > 0) {
     cmdId = Serial.parseInt();
     cmdVal = Serial.parseInt(); 
     if (Serial.read() == '\n') {
        if( cmdId >= 0 && cmdId <= 3 ) {
          pwm.setPWM(cmdId, 0, cmdVal);
        } else if ( cmdId == 9 ) {
          analogWrite(LASER_PIN, cmdVal);
        }
     }
  }
}
